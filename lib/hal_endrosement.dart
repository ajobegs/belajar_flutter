import 'package:flutter/material.dart';

class Endorse extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(20),),
            Text('ENDORSEMENT', style: TextStyle(fontSize: 30)),
            Padding(padding: EdgeInsets.all(20),),
            Icon(Icons.restaurant_menu, size: 100.0,color: Colors.grey,),
          ],
        ),
      ),
    );
  }
}