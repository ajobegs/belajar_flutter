import 'package:flutter/material.dart';

class Admin extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Padding(padding: EdgeInsets.all(20),),
            Text('ADMIN', style: TextStyle(fontSize: 30)),
            Padding(padding: EdgeInsets.all(20),),
            Icon(Icons.mic, size: 100.0,color: Colors.grey,),
          ],
        ),
      ),
    );
  }
}