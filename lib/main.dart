import 'package:flutter/material.dart';
import 'package:ihendis/hal_admin.dart';
import 'package:ihendis/hal_endrosement.dart';
import 'package:ihendis/hal_medical.dart';
import 'package:ihendis/hal_training.dart';

void main() {
  runApp(new MaterialApp(
    title: 'iHENDIS APPS',
    home: new Home(),
  ));
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with SingleTickerProviderStateMixin {
  TabController controller;

  @override
  void initState() {
    controller = TabController(vsync: this, length: 4);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.purple,
        title: Text('Daftar Online'),
        bottom: TabBar(
          controller: controller,
          tabs: <Widget>[
            Tab(
              icon: Icon(Icons.mic),
              text: 'Admin',
            ),
            Tab(
              icon: Icon(Icons.restaurant_menu),
              text: 'Endorsement',
            ),
            Tab(
              icon: Icon(Icons.directions_boat),
              text: 'Medical',
            ),
            Tab(
              icon: Icon(Icons.train),
              text: 'Training',
            ),
          ],
        ),
      ),
      body: TabBarView(
        controller: controller,
        children: <Widget>[Admin(), Endorse(), Medical(), Training()],
      ),
      bottomNavigationBar: Material(
        color: Colors.indigo,
        child: TabBar(
          controller: controller,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.mic)),
            Tab(icon: Icon(Icons.restaurant_menu)),
            Tab(icon: Icon(Icons.directions_boat)),
            Tab(icon: Icon(Icons.train)),
          ],
        ),
      ),
    );
  }
}
// Tutorial 3
// void main() {
//   runApp(new MaterialApp(
//     title: 'iHENDIS APPS',
//     home: new Page1(),
//     routes: <String,WidgetBuilder>{
//       '/page1' : (BuildContext context) => Page1(),
//       '/page2' : (BuildContext context) => Page2(),
//       '/page3' : (BuildContext context) => Page3(),
//     },
//   ));
// }
// class Page1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("SPORT"),
//       ),
//       body: Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.directions_run,
//             size: 70,
//             color: Colors.grey,
//           ),
//           onPressed: (){
//             Navigator.pushNamed(context, '/page2');
//           },
//         ),
//       ),
//     );
//   }
// }

// class Page2 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("MUSIC"),
//       ),
//       body: Center(
//         child: IconButton(
//           icon: Icon(
//             Icons.speaker,
//             size: 70,
//             color: Colors.pink,
//           ),
//           onPressed: (){
//             Navigator.pushNamed(context, '/page3');
//           },
//         ),
//       ),
//     );
//   }
// }
// /// Tutorial 2
// class Page3 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         // leading: Icon(Icons.check),
//         title: Text("Card and Parsing data"),
//       ),
//       body: Container(
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.stretch,
//           children: <Widget>[
//             MyCard(ikon: Icons.home, teks: 'Home',warnaIkon: Colors.lime,),
//             MyCard(ikon: Icons.chat, teks: 'Chat',warnaIkon: Colors.amber,),
//             MyCard(ikon: Icons.note, teks: 'Data Entry',warnaIkon: Colors.orange,),
//             MyCard(ikon: Icons.settings, teks: 'settings',warnaIkon: Colors.red,),
//           ],
//         ),
//       ),
//     );
//   }
// }

// class MyCard extends StatelessWidget {
//   final IconData ikon;
//   final String teks;
//   final Color warnaIkon;

//   MyCard({Key key, this.ikon, this.teks, this.warnaIkon}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       padding: EdgeInsets.all(4.0),
//       child: Card(
//           child: Column(
//         children: <Widget>[
//           Icon(
//             ikon,
//             size: 70,
//             color: warnaIkon,
//           ),
//           Text(
//             teks,
//             style: TextStyle(fontSize: 20),
//           ),
//         ],
//       )),
//     );
//   }
// }

/// Tutorial 1
// class IndexMain extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       backgroundColor: Colors.grey[50],
//       appBar: AppBar(
//         leading: Icon(Icons.home),
//         backgroundColor: Colors.green,
//         title: Center(child: Text('IHENDIS')),
//         actions: <Widget>[Icon(Icons.search)],
//       ),
//       body: Container(
//           child: Column(
//         children: <Widget>[
//           Row(
//             children: <Widget>[
//               Icon(
//                 Icons.settings,
//                 size: 80,
//                 color: Colors.pink,
//               ),
//               Icon(
//                 Icons.home,
//                 size: 80,
//                 color: Colors.pink,
//               ),
//               Column(
//                 children: <Widget>[
//                   Icon(
//                     Icons.cake,
//                     size: 80,
//                     color: Colors.pink,
//                   ),
//                   Icon(
//                     Icons.priority_high,
//                     size: 80,
//                     color: Colors.pink,
//                   ),
//                 ],
//               )
//             ],
//           ),
//           Icon(
//             Icons.face,
//             size: 80,
//             color: Colors.pink,
//           ),
//           Icon(
//             Icons.dashboard,
//             size: 80,
//             color: Colors.pink,
//           ),
//         ],
//       )),
//       // Center(
//       //   child: Container(
//       //     color: Colors.green[900],
//       //     width: 200.0,
//       //     height: 100.0,
//       //     child: Center(
//       //       child:
//       //       Icon(Icons.android,color: Colors.white,size: 70,
//       //       // Text(
//       //       //   'Center text',
//       //       //   style: TextStyle(
//       //       //       color: Colors.white, fontFamily: "Serif", fontSize: 20),
//       //       // ),
//       //     ),
//       //   ),
//       // ),
//       // ),
//     );
//   }
// }
